public class ArrayList<T> implements List<T> {

    private final static int DEFAULT_ARRAY_SIZE = 10;
    private T[] elements;
    private int count;

    @SuppressWarnings("unchecked")
    public ArrayList() {
        this.elements = (T[]) new Object[DEFAULT_ARRAY_SIZE];
        this.count = 0;
    }

    @Override
    public void add(T element) {
        if (isFull()) {
            resize();
        }
        elements[count] = element;
        count++;
    }

    @SuppressWarnings("unchecked")
    private void resize() {
        int currentLength = elements.length;
        int newLength = currentLength + currentLength / 2;
        T[] newElements = (T[]) new Object[newLength];
        System.arraycopy(elements, 0, newElements, 0, count);
        elements = newElements;
    }

    private boolean isFull() {
        return count == elements.length;
    }

    @Override
    public boolean contains(T element) {
        for (int i = 0; i < count; i++) {
            if (elements[i] == element) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public T get(int index) {
        if (index >= 0 && index < count) {
            return elements[index];
        }
        return null;
    }

    @Override
    public void remove(T element) {
        int index;
        for (int i = 0; i < count; i++) {
            if (elements[i] == element) {
                index = i;
                removeAt(index);
                break;
            }
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public void removeAt(int index) {
        if (index >= 0 && index < count) {
            for (int i = index; i < count - 1; i++) {
                elements[i] = elements[i + 1];
            }
            count--;
            T[] newElements = (T[]) new Object[count];
            System.arraycopy(elements, 0, newElements, 0, count);
            elements = newElements;
        }
    }

    @Override
    public String toString() {
        StringBuilder array = new StringBuilder();
        String result;
        for (int i = 0; i < count; i++) {
            array.append(elements[i]).append(" ");
        }
        result = String.valueOf(array);
        return result;
    }
}

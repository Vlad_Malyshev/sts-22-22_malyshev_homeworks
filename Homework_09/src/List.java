public interface List<T> extends Collection<T> {
    void removeAt(int index);

    T get(int index);
}


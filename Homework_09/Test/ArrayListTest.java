import org.junit.Assert;
import org.junit.Test;

public class ArrayListTest {
    @Test
    public void testArrayListForRemove() {
        ArrayList<Integer> integerArrayList = new ArrayList<>();

        integerArrayList.add(8);
        integerArrayList.add(10);
        integerArrayList.add(11);
        integerArrayList.add(13);
        integerArrayList.add(11);
        integerArrayList.add(15);
        integerArrayList.add(-5);

        integerArrayList.remove(11);

        String expectedResult = "8 10 13 11 15 -5 ";

        Assert.assertEquals(expectedResult, integerArrayList.toString());
    }

    @Test
    public void testArrayListForRemoveAt() {
        ArrayList<Integer> integerArrayList = new ArrayList<>();

        integerArrayList.add(8);
        integerArrayList.add(10);
        integerArrayList.add(11);
        integerArrayList.add(13);
        integerArrayList.add(11);
        integerArrayList.add(15);
        integerArrayList.add(-5);

        integerArrayList.removeAt(4);

        String expectedResult = "8 10 11 13 15 -5 ";

        Assert.assertEquals(expectedResult, integerArrayList.toString());
    }
}

import org.junit.Assert;
import org.junit.Test;

public class LinkedListTest {
    @Test
    public void testLinkedListForRemove() {
        LinkedList<Integer> integerLinkedList = new LinkedList<>();

        integerLinkedList.add(8);
        integerLinkedList.add(10);
        integerLinkedList.add(11);
        integerLinkedList.add(13);
        integerLinkedList.add(11);
        integerLinkedList.add(15);
        integerLinkedList.add(-5);

        integerLinkedList.remove(11);

        String expectedResult = "8 10 13 11 15 -5 ";

        Assert.assertEquals(expectedResult, integerLinkedList.toString());
    }

    @Test
    public void testLinkedListForRemoveAt() {
        LinkedList<Integer> integerLinkedList = new LinkedList<>();

        integerLinkedList.add(8);
        integerLinkedList.add(10);
        integerLinkedList.add(11);
        integerLinkedList.add(13);
        integerLinkedList.add(11);
        integerLinkedList.add(15);
        integerLinkedList.add(-5);

        integerLinkedList.removeAt(4);

        String expectedResult = "8 10 11 13 15 -5 ";

        Assert.assertEquals(expectedResult, integerLinkedList.toString());
    }
}

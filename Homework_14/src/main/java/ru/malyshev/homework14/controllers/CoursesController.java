package ru.malyshev.homework14.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.malyshev.homework14.dto.CourseForm;
import ru.malyshev.homework14.services.CoursesService;

@Controller
@RequiredArgsConstructor
public class CoursesController {

    private final CoursesService coursesService;

    @GetMapping("/courses")
    public String getUsersPage(Model model) {
        model.addAttribute("courses", coursesService.getAllCourses());
        return "courses";
    }

    @GetMapping("courses/{course-id}/delete")
    public String deleteCourse(@PathVariable("course-id") Long id) {
        coursesService.deleteUser(id);
        return "redirect:/courses";
    }

    @GetMapping("/courses/{course-id}")
    public String getCoursePage(@PathVariable("course-id") Long courseId,
                                Model model) {
        model.addAttribute("course", coursesService.getCourse(courseId));
        model.addAttribute("notInCourseStudents", coursesService.getNotInCourseStudents(courseId));
        model.addAttribute("inCourseStudents", coursesService.getInCourseStudents(courseId));
        model.addAttribute("notInCourseLessons", coursesService.getNotInCourseLessons());
        model.addAttribute("inCourseLessons", coursesService.getInCourseLessons(courseId));
        return "course";
    }

    @PostMapping("/courses/{course-id}/students")
    public String addStudentToCourse(@PathVariable("course-id") Long courseId,
                                     @RequestParam("student-id") Long studentId) {
        coursesService.addStudentToCourse(studentId, courseId);
        return "redirect:/courses/{course-id}";
    }

    @PostMapping("/courses/{course-id}/lessons")
    public String addLessonToCourse(@PathVariable("course-id") Long courseId,
                                    @RequestParam("lesson-id") Long lessonId) {
        coursesService.addLessonToCourse(lessonId, courseId);
        return "redirect:/courses/{course-id}";
    }

    @PostMapping("/courses")
    public String addCourse(CourseForm course) {
        coursesService.addCourse(course);
        return "redirect:/courses";
    }

    @PostMapping("/courses/{course-id}/update")
    public String updateCourse(@PathVariable("course-id") Long courseId,
                               CourseForm course) {
        coursesService.updateCourse(courseId, course);
        return "redirect:/courses/{course-id}";
    }
}

package ru.malyshev.homework14.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.malyshev.homework14.dto.UserForm;
import ru.malyshev.homework14.services.UsersService;

@Controller
@RequiredArgsConstructor
public class UsersController {

    private final UsersService usersService;

    @GetMapping("/users")
    public String getUsersPage(Model model) {
        model.addAttribute("users", usersService.getAllUsers());
        return "users";
    }

    @GetMapping("/users/{user-id}")
    public String gerUserPage(@PathVariable("user-id") Long id,
                              Model model) {
        model.addAttribute("user", usersService.getUser(id));
        return "user";
    }

    @GetMapping("users/{user-id}/delete")
    public String deleteUser(@PathVariable("user-id") Long id) {
        usersService.deleteUser(id);
        return "redirect:/users";
    }

    @PostMapping("/users")
    public String addUser(UserForm user) {
        usersService.addUser(user);
        return "redirect:/users";
    }

    @PostMapping("users/{user-id}/update")
    public String updateUser(@PathVariable("user-id") Long id,
                             UserForm user) {
        usersService.updateUser(id, user);
        return "redirect:/users/" + id;
    }
}

package ru.malyshev.homework14.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.malyshev.homework14.models.Course;

import java.util.List;

public interface CoursesRepository extends JpaRepository<Course, Long> {
    List<Course> findAllByStateNot(Course.State state);
}

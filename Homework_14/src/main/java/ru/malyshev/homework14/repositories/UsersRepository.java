package ru.malyshev.homework14.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.malyshev.homework14.models.Course;
import ru.malyshev.homework14.models.User;

import java.util.List;

public interface UsersRepository extends JpaRepository<User, Long> {
    List<User> findAllByStateNot(User.State state);

    List<User> findAllByCoursesNotContainsAndStateNot(Course course, User.State state);

    List<User> findAllByCoursesContainsAndStateNot(Course course, User.State state);
}

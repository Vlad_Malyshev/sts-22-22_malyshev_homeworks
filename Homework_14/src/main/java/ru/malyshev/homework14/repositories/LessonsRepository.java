package ru.malyshev.homework14.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.malyshev.homework14.models.Course;
import ru.malyshev.homework14.models.Lesson;

import java.util.List;

public interface LessonsRepository extends JpaRepository<Lesson, Long> {
    List<Lesson> findAllByStateNot(Lesson.State state);

    List<Lesson> findAllByCourseNullAndStateNot(Lesson.State state);

    List<Lesson> findAllByCourseAndStateNot(Course course, Lesson.State state);
}

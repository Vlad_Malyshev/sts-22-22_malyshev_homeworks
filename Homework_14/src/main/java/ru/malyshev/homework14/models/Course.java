package ru.malyshev.homework14.models;

import lombok.*;

import jakarta.persistence.*;

import java.time.LocalDate;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(exclude = {"lessons", "accounts"})
@Entity
public class Course {

    public enum State {
        READY, NOT_READY, DELETED
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;

    @Column(length = 1000)
    private String description;
    private LocalDate start;
    private LocalDate finish;

    @OneToMany(mappedBy = "course", fetch = FetchType.EAGER)
    private Set<Lesson> lessons;

    @ManyToMany(mappedBy = "courses", fetch = FetchType.EAGER)
    private Set<User> users;

    @Enumerated(value = EnumType.STRING)
    private State state;
}

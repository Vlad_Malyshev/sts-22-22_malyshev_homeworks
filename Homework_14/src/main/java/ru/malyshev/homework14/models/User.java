package ru.malyshev.homework14.models;

import lombok.*;
import org.hibernate.annotations.Check;

import jakarta.persistence.*;
import org.hibernate.annotations.ColumnDefault;

import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(exclude = {"courses"})
@ToString(exclude = {"courses"})
@Entity
@Table(name = "account")
public class User {

    public enum State {
        NOT_CONFIRMED, CONFIRMED, DELETED;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true, nullable = false)
    private String email;

    private String password;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Check(constraints = "age >= 0 and age <= 120")
    @ColumnDefault("20")
    private Integer age = 0;

    @Column(name = "is_worker")
    private Boolean isWorker;

    private Double average;

    @ManyToMany
    @JoinTable(joinColumns = {@JoinColumn(name = "account_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "course_id", referencedColumnName = "id")})
    private Set<Course> courses;

    @Enumerated(value = EnumType.STRING)
    private State state;
}

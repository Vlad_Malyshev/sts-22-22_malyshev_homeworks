package ru.malyshev.homework14.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LessonForm {
    private String name;

    private String summary;
    private LocalTime startTime;
    private LocalTime finishTime;
}

package ru.malyshev.homework14.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CourseForm {
    private String title;

    private String description;
    private LocalDate start;
    private LocalDate finish;
}

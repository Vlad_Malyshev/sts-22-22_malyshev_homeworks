package ru.malyshev.homework14.services;

import ru.malyshev.homework14.dto.LessonForm;
import ru.malyshev.homework14.models.Lesson;

import java.util.List;

public interface LessonsService {

    List<Lesson> getAllLessons();

    void addLesson(LessonForm lesson);

    void deleteLesson(Long lessonId);

    Lesson getLesson(Long lessonId);

    void updateLesson(Long lessonId, LessonForm lesson);
}

package ru.malyshev.homework14.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.malyshev.homework14.dto.LessonForm;
import ru.malyshev.homework14.models.Lesson;
import ru.malyshev.homework14.repositories.LessonsRepository;
import ru.malyshev.homework14.services.LessonsService;

import java.time.LocalTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class LessonsServiceImpl implements LessonsService {

    private final LessonsRepository lessonsRepository;

    @Override
    public List<Lesson> getAllLessons() {
        return lessonsRepository.findAllByStateNot(Lesson.State.DELETED);
    }

    @Override
    public void addLesson(LessonForm lesson) {
        Lesson newLesson = Lesson.builder()
                .state(Lesson.State.NOT_READY)
                .name(lesson.getName())
                .startTime(LocalTime.now())
                .finishTime(LocalTime.now())
                .summary("самые обычные записюльки")
                .build();
        lessonsRepository.save(newLesson);
    }

    @Override
    public void deleteLesson(Long lessonId) {
        Lesson lesson = lessonsRepository.findById(lessonId).orElseThrow();
        lesson.setState(Lesson.State.DELETED);
        lessonsRepository.save(lesson);
    }

    @Override
    public Lesson getLesson(Long lessonId) {
        return lessonsRepository.findById(lessonId).orElseThrow();
    }

    @Override
    public void updateLesson(Long lessonId, LessonForm updateData) {
        Lesson lessonForUpdate = lessonsRepository.findById(lessonId).orElseThrow();
        lessonForUpdate.setSummary(updateData.getSummary());
        lessonForUpdate.setStartTime(updateData.getStartTime());
        lessonForUpdate.setFinishTime(updateData.getFinishTime());
        lessonsRepository.save(lessonForUpdate);
    }
}

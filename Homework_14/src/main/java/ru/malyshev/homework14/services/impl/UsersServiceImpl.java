package ru.malyshev.homework14.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.malyshev.homework14.dto.UserForm;
import ru.malyshev.homework14.models.User;
import ru.malyshev.homework14.repositories.UsersRepository;
import ru.malyshev.homework14.services.UsersService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UsersServiceImpl implements UsersService {

    private final UsersRepository usersRepository;

    @Override
    public List<User> getAllUsers() {
        return usersRepository.findAllByStateNot(User.State.DELETED);
    }

    @Override
    public void addUser(UserForm user) {
        User newUser = User.builder()
                .email(user.getEmail())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .age(0)
                .state(User.State.NOT_CONFIRMED)
                .build();
        usersRepository.save(newUser);
    }

    @Override
    public User getUser(Long id) {
        return usersRepository.findById(id).orElseThrow();
    }

    @Override
    public void updateUser(Long id, UserForm updateData) {
        User userForUpdate = usersRepository.findById(id).orElseThrow();

        userForUpdate.setFirstName(updateData.getFirstName());
        userForUpdate.setLastName(updateData.getLastName());
        userForUpdate.setAge(updateData.getAge());

        usersRepository.save(userForUpdate);
    }

    @Override
    public void deleteUser(Long id) {
        User userForDelete = usersRepository.findById(id).orElseThrow();
        userForDelete.setState(User.State.DELETED);
        usersRepository.save(userForDelete);
    }
}

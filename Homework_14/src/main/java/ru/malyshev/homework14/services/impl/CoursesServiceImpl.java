package ru.malyshev.homework14.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.malyshev.homework14.dto.CourseForm;
import ru.malyshev.homework14.models.Course;
import ru.malyshev.homework14.models.Lesson;
import ru.malyshev.homework14.models.User;
import ru.malyshev.homework14.repositories.CoursesRepository;
import ru.malyshev.homework14.repositories.LessonsRepository;
import ru.malyshev.homework14.repositories.UsersRepository;
import ru.malyshev.homework14.services.CoursesService;

import java.time.LocalDate;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CoursesServiceImpl implements CoursesService {

    private final CoursesRepository coursesRepository;

    private final UsersRepository usersRepository;

    private final LessonsRepository lessonsRepository;

    @Override
    public void addStudentToCourse(Long studentId, Long courseId) {
        Course course = coursesRepository.findById(courseId).orElseThrow();
        User student = usersRepository.findById(studentId).orElseThrow();

        student.getCourses().add(course);

        usersRepository.save(student);
    }

    @Override
    public void addLessonToCourse(Long lessonId, Long courseId) {
        Course course = coursesRepository.findById(courseId).orElseThrow();
        Lesson lesson = lessonsRepository.findById(lessonId).orElseThrow();

        course.getLessons().add(lesson);
        lesson.setCourse(course);

        coursesRepository.save(course);
    }

    @Override
    public Course getCourse(Long id) {
        return coursesRepository.findById(id).orElseThrow();
    }

    @Override
    public List<User> getNotInCourseStudents(Long courseId) {
        Course course = coursesRepository.findById(courseId).orElseThrow();
        return usersRepository.findAllByCoursesNotContainsAndStateNot(course, User.State.DELETED);
    }

    @Override
    public List<Lesson> getNotInCourseLessons() {
        return lessonsRepository.findAllByCourseNullAndStateNot(Lesson.State.DELETED);
    }

    @Override
    public List<Lesson> getInCourseLessons(Long courseId) {
        Course course = coursesRepository.findById(courseId).orElseThrow();
        return lessonsRepository.findAllByCourseAndStateNot(course, Lesson.State.DELETED);
    }

    @Override
    public List<User> getInCourseStudents(Long courseId) {
        Course course = coursesRepository.findById(courseId).orElseThrow();
        return usersRepository.findAllByCoursesContainsAndStateNot(course, User.State.DELETED);
    }

    @Override
    public List<Course> getAllCourses() {
        return coursesRepository.findAllByStateNot(Course.State.DELETED);
    }

    @Override
    public void addCourse(CourseForm course) {
        Course newCourse = Course.builder()
                .title(course.getTitle())
                .description("Обычный курс")
                .start(LocalDate.now())
                .finish(LocalDate.now())
                .state(Course.State.NOT_READY)
                .build();
        coursesRepository.save(newCourse);
    }

    @Override
    public void deleteUser(Long id) {
        Course course = coursesRepository.findById(id).orElseThrow();
        course.setState(Course.State.DELETED);
        coursesRepository.save(course);
    }

    @Override
    public void updateCourse(Long courseId, CourseForm updateData) {
        Course courseForUpdate = coursesRepository.findById(courseId).orElseThrow();
        courseForUpdate.setDescription(updateData.getDescription());
        courseForUpdate.setStart(updateData.getStart());
        courseForUpdate.setFinish(updateData.getFinish());
        coursesRepository.save(courseForUpdate);
    }
}

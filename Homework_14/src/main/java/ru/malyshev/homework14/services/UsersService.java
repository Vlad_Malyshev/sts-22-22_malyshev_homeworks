package ru.malyshev.homework14.services;

import ru.malyshev.homework14.dto.UserForm;
import ru.malyshev.homework14.models.User;

import java.util.List;

public interface UsersService {
    List<User> getAllUsers();

    void addUser(UserForm user);

    User getUser(Long id);

    void updateUser(Long id, UserForm user);

    void deleteUser(Long id);
}

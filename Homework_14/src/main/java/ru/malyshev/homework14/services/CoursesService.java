package ru.malyshev.homework14.services;

import ru.malyshev.homework14.dto.CourseForm;
import ru.malyshev.homework14.models.Course;
import ru.malyshev.homework14.models.Lesson;
import ru.malyshev.homework14.models.User;

import java.util.List;

public interface CoursesService {

    void addStudentToCourse(Long studentId, Long courseId);

    Course getCourse(Long id);

    List<User> getNotInCourseStudents(Long courseId);

    List<User> getInCourseStudents(Long courseId);

    List<Course> getAllCourses();

    void addCourse(CourseForm course);

    void deleteUser(Long id);

    void updateCourse(Long courseId, CourseForm course);

    void addLessonToCourse(Long lessonId, Long courseId);

    List<Lesson> getNotInCourseLessons();

    List<Lesson> getInCourseLessons(Long courseId);
}

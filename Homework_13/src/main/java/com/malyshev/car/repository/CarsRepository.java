package com.malyshev.car.repository;

import com.malyshev.car.models.Car;

import java.util.List;


public interface CarsRepository {
    List<Car> findAll();

    void save(Car car);
}

package com.malyshev.car.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor

public class Car {
    private Integer id;
    private String model;
    private String color;
    private String number;
    private Integer ownerId;
}

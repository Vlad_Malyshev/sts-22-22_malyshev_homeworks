import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        HashMap<String, Integer> map = new HashMap<>();

        String input = scanner.nextLine();

        String[] words = input.split(" ");

        int count = 0;
        String mostPopularWorld = null;

        for (String word : words) {
            int value = 0;
            if (map.get(word) != null) {
                value = map.get(word);
            }
            int valueOfElement = value + 1;
            map.put(word, valueOfElement);
            if (valueOfElement > count) {
                count = valueOfElement;
                mostPopularWorld = word;
            }
        }

        if (mostPopularWorld == null || mostPopularWorld.equals("")) {
            System.err.println("Nothing here");
        } else {
            System.out.println(mostPopularWorld + " " + count);
        }
    }
}
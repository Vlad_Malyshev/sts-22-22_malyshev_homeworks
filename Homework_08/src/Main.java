public class Main {
    public static void main(String[] args) {

        ArrayTask sumArrayFromTo = (array, from, to) -> {
            int sum = 0;
            for (int i = from; i <= to; i++) {
                sum += array[i];
            }
            return sum;
        };

        ArrayTask sumOfDigitsOfMaxNumber = (array, from, to) -> {
            int max = 0;
            int current;
            int sum = 0;
            for (int i = from; i <= to; i++) {
                if (array[i] > max) {
                    max = array[i];
                }
            }
            current = max;
            while (current > 0) {
                sum += current % 10;
                current /= 10;
            }
            return sum;
        };

        int[] array1 = {10, 11, 255, 3040, 7};
        int from1 = 0;
        int to1 = 3;

        ArrayTaskResolver.resolveTask(array1, sumArrayFromTo, from1, to1);

        int[] array2 = {12, 62, 4, 2, 123, 47, 56};
        int from2 = 2;
        int to2 = 5;

        ArrayTaskResolver.resolveTask(array2, sumOfDigitsOfMaxNumber, from2, to2);
    }
}
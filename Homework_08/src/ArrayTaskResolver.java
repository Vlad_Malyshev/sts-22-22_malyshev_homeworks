import java.util.Arrays;

public class ArrayTaskResolver {
    public static void resolveTask(int[] array, ArrayTask task, int from, int to) {
        System.out.println(Arrays.toString(array));
        System.out.println("From - " + from + "\nTo - " + to);
        System.out.println("Result: " + task.resolve(array, from, to));
    }
}

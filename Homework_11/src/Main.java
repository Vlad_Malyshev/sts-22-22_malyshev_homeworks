public class Main {
    public static void main(String[] args) {
        CarRepository carRepository = new CarRepositoryFileBasedImpl("car.txt");
        Car jigul = new Car("x897ep", "jigul", "blue", 325_689.5, 50_000);
        carRepository.save(jigul);
        System.out.println(carRepository.findAll() + "\n\n");
        System.out.println(carRepository.findNumberByColorOrMileage("Black", 0) + "\n\n");
        System.out.println(carRepository.findAllUniqueCarInRangeOfPrice(70_000, 85_000) + "\n\n");
        System.out.println(carRepository.findColorByMinimalPrice() + "\n\n");
        System.out.println(carRepository.findAveragePriceOfCar("Camry") + "\n\n");
    }
}
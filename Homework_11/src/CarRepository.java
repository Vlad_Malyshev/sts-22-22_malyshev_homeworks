import java.util.List;

public interface CarRepository {
    List<Car> findAll();

    void save(Car car);

    List<String> findNumberByColorOrMileage(String color, double mileage);

    long findAllUniqueCarInRangeOfPrice(int priceFrom, int priceTo);

    String findColorByMinimalPrice();

    double findAveragePriceOfCar(String model);
}

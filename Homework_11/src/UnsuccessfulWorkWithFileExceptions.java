public class UnsuccessfulWorkWithFileExceptions extends RuntimeException {
    public UnsuccessfulWorkWithFileExceptions(Exception e) {
        super(e);
    }
}

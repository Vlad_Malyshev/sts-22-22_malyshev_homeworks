public class EvenNumbersPrintTask extends AbstractNumbersPrintTask {

    EvenNumbersPrintTask(int from, int to) {
        super(from, to);
    }

    @Override
    public void complete() {
        System.out.print("Even elements of range: ");
        for (int i = from; i <= to; i++) {
            if (i % 2 == 0) {
                System.out.print(i + " ");
            }
        }
        System.out.print("\n");
    }
}

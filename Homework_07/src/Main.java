public class Main {

    public static void completeAllTask(Task[] tasks) {
        for (Task task : tasks) {
            task.complete();
        }
    }

    public static void main(String[] args) {

        EvenNumbersPrintTask e1 = new EvenNumbersPrintTask(0, 10);
        EvenNumbersPrintTask e2 = new EvenNumbersPrintTask(18, 23);
        EvenNumbersPrintTask e3 = new EvenNumbersPrintTask(36, 67);

        OddNumbersPrintTask o1 = new OddNumbersPrintTask(0, 10);
        OddNumbersPrintTask o2 = new OddNumbersPrintTask(13, 37);
        OddNumbersPrintTask o3 = new OddNumbersPrintTask(51, 75);

        Task[] tasks = {e1, e2, e3, o1, o2, o3};
        completeAllTask(tasks);
    }
}
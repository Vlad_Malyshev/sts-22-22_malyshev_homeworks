import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        try {
            ATM atm = new ATM(10_000);

            atm.give();

            System.out.println("\nВ банкомате осталось: " + atm.getLeftAmount() + "\n");

            double valueToPull;
            System.out.print("Введите сумму которое вы хотите положить: ");
            valueToPull = scanner.nextDouble();

            atm.pull(valueToPull);

            System.out.println("\n\nКол-во проведённых операций: " + atm.getQuantity() + "\nВ банкомате осталось: " + atm.getLeftAmount());
        } catch (InputMismatchException e) {
            System.err.println("Ввод некорректных значений!!! Вводите ЦЕЛОЧИСЛЕННЫЕ значения");
        }
    }
}
import org.junit.Assert;
import org.junit.Test;

public class TestFigure {
    @Test
    public void testFigureForMove() {
        Square square = new Square(5);

        Figure[] figures = {square};

        for (int i = 0; i < figures.length; i++) {
            figures[i].move(5, -7);
        }

        double squareX = 5;
        double squareY = -7;

        Assert.assertEquals(squareX, square.getX(), 0.0001);
        Assert.assertEquals(squareY, square.getY(), 0.0001);
    }
}

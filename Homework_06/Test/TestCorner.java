import org.junit.Assert;
import org.junit.Test;

public class TestCorner {
    @Test
    public void testCornerForChangeGrad() {

        Square square = new Square(5);
        Rectangle rectangle = new Rectangle(4, 6);

        Corner[] corners = {square, rectangle};

        for (int i = 0; i < corners.length; i++) {
            corners[i].changeCorner(-15.5);
        }

        double cornerSquare = 74.5;
        double cornerRectangle = 74.5;

        Assert.assertEquals(cornerSquare, square.getCorner(), 0.00001);
        Assert.assertEquals(cornerRectangle, rectangle.getCorner(), 0.00001);
    }
}

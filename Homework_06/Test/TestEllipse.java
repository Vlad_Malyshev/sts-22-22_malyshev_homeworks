import org.junit.Assert;
import org.junit.Test;

public class TestEllipse {
    @Test
    public void testEllipseForArea() {
        Ellipse ellipse = new Ellipse(3, 7);
        Circle circle = new Circle(7);

        double ellipseAreaWanted = 65.94;

        double circleAreaWanted = 153.86;

        Assert.assertEquals(ellipseAreaWanted, ellipse.area(), 0.00001);
        Assert.assertEquals(circleAreaWanted, circle.area(), 0.00001);
    }

    @Test
    public  void testEllipseForPerimeter() {
        Ellipse ellipse = new Ellipse(3, 7);
        Circle circle = new Circle(7);

        double ellipsePerimeterWanted = 27.976;

        double circlePerimeterWanted = 43.96;

        Assert.assertEquals(ellipsePerimeterWanted, ellipse.perimeter(), 0.00001);
        Assert.assertEquals(circlePerimeterWanted, circle.perimeter(), 0.00001);
    }
}

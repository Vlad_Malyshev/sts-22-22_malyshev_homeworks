import org.junit.Assert;
import org.junit.Test;

public class TestRectangle {
    @Test
    public void testRectangleForArea() {
        Square square = new Square(5);
        Rectangle rectangle = new Rectangle(4, 6);

        double rectangleAreaWanted = 24;

        double squareAreaWanted = 25;

        Assert.assertEquals(rectangleAreaWanted, rectangle.area(), 0.00001);
        Assert.assertEquals(squareAreaWanted, square.area(), 0.00001);
    }

    @Test
    public void testRectangleForPerimeter() {
        Square square = new Square(5);
        Rectangle rectangle = new Rectangle(4, 6);

        double rectanglePerimeterWanted = 20;

        double squarePerimeterWanted = 20;

        Assert.assertEquals(rectanglePerimeterWanted, rectangle.perimeter(), 0.00001);
        Assert.assertEquals(squarePerimeterWanted, square.perimeter(), 0.00001);
    }
}

public abstract class Figure {
    private double x = 0;
    private double y = 0;

    public void move(int x_move, int y_move) {
        x += x_move;
        y += y_move;
    }

    public abstract double perimeter();

    public abstract double area();

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }
}

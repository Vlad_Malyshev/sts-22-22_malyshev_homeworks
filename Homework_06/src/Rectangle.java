public class Rectangle extends Figure implements Corner {

    protected final double a;
    protected final double b;

    private double corner = 90;

    Rectangle(double a, double b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public double perimeter() {
        return (a + b) * 2;
    }

    @Override
    public double area() {
        return a * b;
    }

    public double getCorner() {
        return corner;
    }

    @Override
    public void changeCorner(double changingGrad) {
        corner += changingGrad;
    }
}

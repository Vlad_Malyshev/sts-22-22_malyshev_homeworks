public class Ellipse extends Figure {
    protected double r_min = 1;
    protected double r_max = 1;

    Ellipse(double r_min, double r_max) {
        this.r_min = r_min;
        this.r_max = r_max;
    }

    @Override
    public double perimeter() {
        return 4 * ((3.14 * r_min * r_max + (r_max - r_min)) / (r_min + r_max));
    }

    @Override
    public double area() {
        return r_min * r_max * 3.14;
    }
}

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean k = true;
        int length = 0;
        int k_min = 0;

        while(k) {
            System.out.print("Введите размер массива: ");
            length = scanner.nextInt();
            if (length <= 0) {
                System.out.println("Такого массива не существует! Введите другое значение!");
            } else {
                k = false;
            }
        }

        int[] array = new int[length];

        if(length == 1) {
            System.out.print("Введите 0 эллемент массива: ");
            array[0] = scanner.nextInt();
            System.out.println("Колл-во локальных минимумов: 1");
            System.out.println("Массив: ");
            System.out.println(array[0]);
        } else {
            for (int i = 0; i < length; i++) {
                System.out.print("Введите " + i + " эллемент массива: ");
                array[i] = scanner.nextInt();
            }

            for (int i = 0; i < length; i++) {
                if (i == 0) {
                    if (array[i] < array[i + 1])
                        k_min++;
                } else if (i == length - 1) {
                    if (array[i] < array[i - 1])
                        k_min++;
                } else if (array[i] < array[i - 1] && array[i] < array[i + 1]) {
                    k_min++;
                }
            }
            System.out.println("Колл-во локальных минимумов: " + k_min);
            System.out.println("Массив: ");
            for (int i = 0; i < length; i++) {
                System.out.print(array[i] + " ");
            }
        }
    }
}
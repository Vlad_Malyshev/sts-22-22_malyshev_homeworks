package exeptions;

public class UnsuccessfulWorkWithWriterException extends RuntimeException {
    public UnsuccessfulWorkWithWriterException(Throwable e) {
        super(e);
    }
}

package exeptions;

public class UnsuccessfulWorkWithReaderException extends RuntimeException {
    public UnsuccessfulWorkWithReaderException(Throwable e) {
        super(e);
    }
}

package repository;

import java.util.List;

import domain.Product;

public interface ProductRepository {
    Product findById(int id);

    List<Product> findAllByTitleLike(String title);

    void update(Product product);
}

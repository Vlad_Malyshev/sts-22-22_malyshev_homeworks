package repository;

import domain.Product;
import exeptions.*;

import java.io.*;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ProductRepositoryFileBasedImpl implements ProductRepository {

    private final String fileName;

    public ProductRepositoryFileBasedImpl(String fileName) {
        this.fileName = fileName;
    }

    private static final Function<String, Product> stringToProductMapper = current -> {
        String[] parts = current.split("\\|");
        int id = Integer.parseInt(parts[0]);
        String title = parts[1];
        double price = Double.parseDouble(parts[2]);
        int quantityInStock = Integer.parseInt(parts[3]);

        return new Product(id, title, price, quantityInStock);
    };

    private static final Function<Product, String> productToStringMapper = product -> product.getId() + "|" +
            product.getTitle() + "|" +
            product.getCost() + "|" +
            product.getQuantityInStock();

    @Override
    public Product findById(int id) {
        try (FileReader fileReader = new FileReader(fileName);
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            return bufferedReader
                    .lines()
                    .map(stringToProductMapper)
                    .filter(it -> it.getId() == id)
                    .min(Comparator.comparingInt(Product::getId))
                    .orElse(null);
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithReaderException(e);
        }
    }

    @Override
    public List<Product> findAllByTitleLike(String title) {
        try (FileReader fileReader = new FileReader(fileName);
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            return bufferedReader
                    .lines()
                    .map(stringToProductMapper)
                    .filter(product -> product.getTitle().contains(title))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithReaderException(e);
        }
    }

    @Override
    public void update(Product product) {

        List<Product> previousProductList;

        try (FileReader fileReader = new FileReader(fileName);
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {

            previousProductList = bufferedReader
                    .lines()
                    .map(stringToProductMapper)
                    .collect(Collectors.toList());

            for (Product productLast : previousProductList) {
                if (productLast.getId() == product.getId()) {
                    previousProductList.set(previousProductList.indexOf(productLast), product);
                }
            }
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithReaderException(e);
        }

        try (FileWriter fileWriter = new FileWriter(fileName, false);
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {
            for (Product productNew : previousProductList) {
                String newProduct = productToStringMapper.apply(productNew);
                bufferedWriter.write(newProduct);
                bufferedWriter.newLine();
            }
        } catch (IOException e) {
            throw new UnsuccessfulWorkWithWriterException(e);
        }
    }
}
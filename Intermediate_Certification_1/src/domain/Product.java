package domain;

import java.util.Objects;

public class Product {
    private final int id;
    private String title;
    private double cost;
    private int quantityInStock;

    public Product(int id, String title, double cost, int quantityInStock) {
        this.id = id;
        this.title = title;
        this.cost = cost;
        this.quantityInStock = quantityInStock;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public void setQuantityInStock(int quantityInStock) {
        this.quantityInStock = quantityInStock;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public double getCost() {
        return cost;
    }

    public int getQuantityInStock() {
        return quantityInStock;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", cost=" + cost +
                ", quantityInStock=" + quantityInStock +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Product product)) return false;
        return id == product.id && Double.compare(product.cost, cost) == 0 && quantityInStock == product.quantityInStock && Objects.equals(title, product.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, cost, quantityInStock);
    }
}

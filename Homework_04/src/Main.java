import java.util.Scanner;

public class Main {

    public static int getSumInInterval(int from, int to){
        int sum=0;
        if(from > to){
            sum = -1;
        } else if (from == to) {
            sum = from;
        } else {
            for (int i = from ; i <= to; i++) {
                sum+=i;
            }
        }
        return sum;
    }

    public static void evensOfArray(int[] a) {
        System.out.println("\nEvens elements of array: ");
        for (int i = 0; i < a.length; i++) {
            if(a[i] % 2 == 0){
                System.out.print(a[i] + " ");
            }
        }
    }

    public static int toInt(int[] a){
        int number=0;
        for (int i = 0; i < a.length; i++) {
            number = number * 10 + a[i];
        }
        return number;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        //Первая чать ДЗ
        int myFrom, myTo, resultOfSum;

        System.out.print("\nEnter border from: ");
        myFrom = scanner.nextInt();
        System.out.print("Enter border to: ");
        myTo = scanner.nextInt();

        resultOfSum=getSumInInterval(myFrom, myTo);
        System.out.println("Sum of the numbers in the interval: " + resultOfSum);

        //Вторая часть ДЗ
        int[] evenArray = {9, 8, 7, 2, 3};
        evensOfArray(evenArray);

        //Третья часть ДЗ
        int[] numberArray = {9, 8, 7, 2, 3};
        int result = toInt(numberArray);
        System.out.println("\n\nNumber from array: " + result);
    }
}
insert into person (age, have_license)
values (21, true),
       (22, false);

insert into person (age, have_license, experience)
values (22, true, 15);

insert into person (age, have_license, license_category)
values (22, true, 'D');

insert into person (first_name, last_name, phone_number, experience, age, have_license, license_category, rating)
values ('Vladislav', 'Malyshev', '+78005553535', 0, 20, false, 'C', 5);

-------------------------------------------------------------------------------------------------------------------

insert into car (model, color, number, owner_id)
values ('VAZ 2107', 'blue', 'x678er777', 5);

insert into car (model, number, owner_id)
values ('Houndai Solaris', 'm123ir888', 1),
       ('Kia rio', 'r321im999', 2),
       ('Shcoda Rapid', 'b456ro770', 3),
       ('Camry 3.5', 'o789oo', 4);

---------------------------------------------------------------------------------------------------------------------

insert into ride (driver_id, car_id, ride_date, ride_interval)
values (5, 1, '2022-11-03', '1:00'),
       (1, 3, '2022-10-10', '0:30'),
       (2, 4, '2022-10-25', '1:25'),
       (3, 4, '2022-11-15', '0:45'),
       (4, 5, '2022-11-12', '2:40');